#include "ItemMenu.h"
#include <iostream>

using namespace OPI;

ItemMenu::ItemMenu(char* _item_name, Func _func) : AbstractItemMenu(_item_name, _func) {}

char* ItemMenu::getItemName() { return m_item_name; }

void ItemMenu::printItemName() {
	std::cout << m_item_name << std::endl;
}

int ItemMenu::run() { return m_func(); }