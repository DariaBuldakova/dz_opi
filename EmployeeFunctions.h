#pragma once
#include "Employee.h"
#include <fstream>
#include <iterator>
#include <algorithm>
#include "ItemMenu.h"
#include "CMenu.h"
using namespace OPI;

int addEmployee();

int watchOneEmployee();
int watchAllEmployees();
int deleteEmployee();
int deleteName();
int deleteId();
int deleteLogin();

void inFile();
void inVector();

int search();
int searchName();
int searchLogin();
int searchId();
int searchPosition();
int searchExperience();
int sort();
extern vector <Employee> vEmployee;
