#pragma once

#include <iostream>
#include "ItemMenu.h"

namespace OPI
{
	class CMenu
	{
	public:
		CMenu(char*, ItemMenu*, size_t);
		~CMenu();

		int getSelect() const;
		bool isRunning() const;
		char* getTitle();
		size_t getCount() const;
		ItemMenu* getItems();

		void printItems() const;
		int runCommand();

		friend std::ostream& operator<<(std::ostream&, const CMenu&);
		friend std::istream& operator>>(std::istream&, CMenu&);

	private:
		int m_select{ -1 };
		bool m_running{};
		char* m_title{};
		size_t m_count{};
		ItemMenu* m_items{};
	};
}

