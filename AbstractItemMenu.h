#pragma once
const int LTITLE = 30;

namespace OPI
{
	typedef int(*Func)();

	class AbstractItemMenu
	{
	public:

		AbstractItemMenu(char*, Func);

		virtual char* getItemName() = 0;
		virtual void printItemName() = 0;
		virtual int run() = 0;

	protected:
		char* m_item_name{};
		Func m_func{};
	};
}

