#include "EmployeeFunctions.h"

vector <Employee> vEmployee{};

#pragma region add
int addEmployee() {

	Employee e{};
	cin >> e;

	vEmployee.push_back(e);
	
    inFile();
	
	cout << "Пользователь добавлен" << endl;
	system("pause");
	return 3;
}
#pragma endregion
#pragma region informarion
int watchOneEmployee()
{
	if (vEmployee.size() != 0)
	{
		for (size_t i = 0; i < vEmployee.size(); i++)
		{
			cout << i + 1 << ": " << vEmployee[i].getSurname() << " " << vEmployee[i].getName() << endl;
		}
		int value{};
		cout << "Выберите пользователя, о котором хотите посмотреть информацию:\t";
		cin >> value;

		system("cls");

		if (value <1 || value>vEmployee.size())
			cout << "Вы ввели что-то не то..." << endl;
		else
			vEmployee[value - 1].print();

		
	}
	else
	{
		cout << "Список сотрудников пуст" << endl;
	}

	system("pause");
	return 3;
}

int watchAllEmployees() {
	if (vEmployee.size() == 0)
	{
		cout << "Список сотрудников пуст" << endl;
	}
	else
	{
		for (int i = 0; i < vEmployee.size(); i++)
			cout << i + 1 << ": " << vEmployee[i];
		cout << endl;
	}
	system("pause");
	return 3;
}
#pragma endregion

#pragma region delete
int deleteEmployee()
{
	if (vEmployee.size() != 0) {

		ItemMenu deleteMenu[3]{ ItemMenu((char*)"имени и фамилии", deleteName),
	ItemMenu((char*)"логину", deleteLogin),
	ItemMenu((char*)"id", deleteId) };

		CMenu menuDelete((char*)"Удалить по:", deleteMenu, 3);
		while (menuDelete.runCommand()) { system("pause"); system("cls"); };
	}
	else
	{
		cout << "Список сотрудников пуст" << endl;
		system("pause");
	}

	return 3;
}


int deleteName()
{
	string name{}, surname{};
	vector<Employee>::iterator it = vEmployee.begin();
	int size = vEmployee.size();

	cout << "Введите фамилию:\t";
	cin >> surname;
	cout << "Введите имя:\t";
	cin >> name;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++)
	{
		if (vEmployee[i].getName() == name && vEmployee[i].getSurname() == surname) {
			vEmployee.erase(it);
		}
		else {
			it++;
		}
	}

	if (vEmployee.size() == size) {
		cout << "Сотрудник не найден" << endl;
	}
	else
	{
		cout << "Сотрудник удален" << endl;
	}
	return 1;
}

int deleteLogin()
{
	string login{};
	vector<Employee>::iterator it = vEmployee.begin();
	int size = vEmployee.size();

	cout << "Введите логин:\t";
	cin >> login;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++)
	{
		if (vEmployee[i].getLogin() == login) {
			vEmployee.erase(it);
			cout << "Сотрудник удален" << endl;
			break;
		}
		else {
			it++;
		}
	}
	if (vEmployee.size() == size) {
		cout << "Сотрудник не найден" << endl;
	}
	return 1;
}

int deleteId()
{
	time_t id{};
	vector<Employee>::iterator it = vEmployee.begin();
	int size = vEmployee.size();

	cout << "Введите id:\t";
	cin >> id;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++)
	{
		if (vEmployee[i].getId() == id) {
			vEmployee.erase(it);
			cout << "Сотрудник удален" << endl;
			break;
		}
		else {
			it++;
		}
	}
	if (vEmployee.size() == size) {
		cout << "Сотрудник не найден" << endl;
	}
	return 1;
}
#pragma endregion

#pragma region search
int search()
{
	if (vEmployee.size() != 0) {

		ItemMenu searchMenu[5]{ ItemMenu((char*)"имени и фамилии", searchName),
	ItemMenu((char*)"логину", searchLogin),
	ItemMenu((char*)"id", searchId),
	ItemMenu((char*)"должности", searchPosition),
	ItemMenu((char*)"стажу", searchExperience)};

		CMenu menuSearch((char*)"Найти по:", searchMenu, 5);
		while (menuSearch.runCommand()) { system("pause"); system("cls"); };
	}
	else
	{
		cout << "Список сотрудников пуст" << endl;
		system("pause");
	}

	return 3;
}

int searchName()
{
	string name{}, surname{};
	int k{};	
	cout << "Введите фамилию:\t";
	cin >> surname;
	cout << "Введите имя:\t";
	cin >> name;
	cout << endl;
 
	for (int i = 0; i < vEmployee.size(); i++) {
		if (vEmployee[i].getName() == name && vEmployee[i].getSurname() == surname) {
			vEmployee[i].print();
			cout << endl;
			k++;
		}
	}

	if (k == 0)
		cout << "Сотрудники не найдены" << endl;

	return 1;
}

int searchLogin()
{
	string login{};
	int k{};
	cout << "Введите логин:\t";
	cin >> login;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++) {
		if (vEmployee[i].getLogin() == login) {
			vEmployee[i].print();
			cout << endl;
			k++;
		}
	}

	if (k == 0)
		cout << "Сотрудники не найдены" << endl;
	return 1;
}

int searchPosition()
{
	string position{};
	int k{};
	cout << "Введите должность:\t";
	cin >> position;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++) {
		if (vEmployee[i].getPosition() == position) {
			vEmployee[i].print();
			cout << endl;
			k++;
		}
	}

	if (k == 0)
		cout << "Сотрудники не найдены" << endl;

	return 1;
}

int searchId()
{
	time_t id{};
	int k{};
	cout << "Введите id:\t";
	cin >> id;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++) {
		if (vEmployee[i].getId() == id) {
			vEmployee[i].print();
			cout << endl;
			k++;
		}
	}

	if (k == 0)
		cout << "Сотрудники не найдены" << endl;
	return 1;
}

int searchExperience()
{
	unsigned int experience{};
	int k{};
	cout << "Введите стаж:\t";
	cin >> experience;
	cout << endl;

	for (int i = 0; i < vEmployee.size(); i++) {
		if (vEmployee[i].getExperience() == experience) {
			vEmployee[i].print();
			cout << endl;
			k++;
		}
	}

	if (k == 0)
		cout << "Сотрудники не найдены" << endl;

	return 1;
}
int sort() 
{
	if (vEmployee.size()) {
		cout << "Сортировать по:\n1 -> фамилии в алфавитном порядке\n2 -> фамилии в обратном алфавитном порядке\n3 -> должности(в алфавитном порядке)\n4 -> стажу\n5 -> зарплате" << endl;
		int value{};
		cin >> value;

		switch (value)
		{
		case 1:
		{
			sort(vEmployee.begin(), vEmployee.end(), [](Employee _employee_1, Employee _employee_2) {
				return _employee_1.getSurname() < _employee_2.getSurname();
				});
			cout << "Сортировка выполнена" << endl;
			break;
		}
		case 2:
		{
			sort(vEmployee.begin(), vEmployee.end(), [](Employee _employee_1, Employee _employee_2) {
				return _employee_1.getSurname() > _employee_2.getSurname();
				});
			cout << "Сортировка выполнена" << endl;
			break;
		}
		case 3:
		{
			sort(vEmployee.begin(), vEmployee.end(), [](Employee _employee_1, Employee _employee_2) {
				return _employee_1.getPosition() < _employee_2.getPosition();
				});
			cout << "Сортировка выполнена" << endl;
			break;
		}
		case 4:
		{
			sort(vEmployee.begin(), vEmployee.end(), [](Employee _employee_1, Employee _employee_2) {
				return _employee_1.getExperience() < _employee_2.getExperience();
				});
			cout << "Сортировка выполнена" << endl;
			break;
		}
		case 5:
		{
			sort(vEmployee.begin(), vEmployee.end(), [](Employee _employee_1, Employee _employee_2) {
				return _employee_1.getSalary() < _employee_2.getSalary();
				});
			cout << "Сортировка выполнена" << endl;
			break;
		}
		default:
			cout << "Вы ввели что-то не то..." << endl;
			break;
		}
		inFile();
	}
	else
	{
		cout << "Список сотрудников пуст" << endl;
	}

	system("pause");
	return 3;
}
#pragma endregion

void inFile()
{
	std::ofstream fout(file, ios::out);
	Employee e{};

	if (fout)
	{
		fout << vEmployee.size() << std::endl;
		for (int i = 0; i < vEmployee.size(); i++)
		{
			e = vEmployee[i];
			fout << e.getSurname() << endl;
			fout << e.getName() << endl;
			fout << e.getLogin() << endl;
			fout << e.getPassword() << endl;
			fout << e.getPosition() << endl;
			fout << e.getMail() << endl;
			fout << e.getId() << endl;
			fout << e.getSalary() << endl;
			fout << e.getExperience() << endl;
		}
	}
	fout.close();
}

void inVector()
{
	std::ifstream fin;
	fin.open(file);

	vEmployee.erase(vEmployee.begin(), vEmployee.end());

	Employee e{};

	unsigned int value{}; 
	int n{};
	string str{};
	time_t id;

	if (fin)
	{
		fin >> n;
		for (int i = 0; i < n; i++)
		{
			fin >> str;
			e.setSurname(str);
			fin >> str;
			e.setName(str);
			fin >> str;
			e.setLogin(str);
			fin >> str;
			e.setPassword(str);
			fin >> str;
			e.setPosition(str);
			fin >> str;
			e.setMail(str);
			fin >> id;
			e.setId(id);
			fin >> value;
			e.setSalary(value);
			fin >> value;
			e.setExperience(value);

			vEmployee.push_back(e);
		}
	}
	fin.close();
}
