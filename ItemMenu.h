#pragma once
#include "AbstractItemMenu.h"

namespace OPI
{
	class ItemMenu : public AbstractItemMenu
	{
	public:
		ItemMenu(char*, Func);
		char* getItemName();
		void printItemName();
		int run();
	};
}


