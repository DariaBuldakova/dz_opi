#include "CMenu.h"
#include <iostream>

using namespace OPI;

CMenu::CMenu(char* title, ItemMenu* items, size_t count) : m_title(title), m_items(items), m_count(count) {}

int CMenu::getSelect() const { return m_select; }
bool CMenu::isRunning() const { return m_running; }
char* CMenu::getTitle() { return m_title; }
size_t CMenu::getCount() const { return m_count; }
ItemMenu* CMenu::getItems() { return m_items; }

void CMenu::printItems() const {
	for (size_t i = 0; i < m_count; ++i) {
		std::cout << i + 1 << " -> ";
		m_items[i].printItemName();
	}
	std::cout << "0 -> ����� (�����)" << std::endl;
}

int CMenu::runCommand() {
	std::cout << m_title << std::endl;
	printItems();
	std::cin >> m_select;
	system("cls");
	if (m_select <= m_count && m_select != 0)
		return m_items[m_select - 1].run();
	else
		return 0;
}

CMenu::~CMenu() {}

namespace OPI {
	std::ostream& operator<<(std::ostream& out, const CMenu& _menu) {
		_menu.printItems();
		return out;
	}

	std::istream& operator>>(std::istream& in, CMenu& _menu) {
		in >> _menu.m_select;
		_menu.m_items[_menu.m_select - 1].run();
		return in;
	}
}

