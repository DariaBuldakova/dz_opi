#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>
using namespace std;

class Employee
{
public:
	Employee();
	~Employee();

	void print();
	
	friend ostream& operator<<(ostream&, const Employee&);
	friend istream& operator>>(istream&, Employee&);

	string getName();
	string getSurname();
	string getLogin();
	string getPassword();
	unsigned int getSalary();
	string getPosition();
	time_t getId();
	unsigned int getExperience();
	string getMail();

	void setName(string);
	void setSurname(string);
	void setLogin(string);
	void setPassword(string);
	void setSalary(unsigned int);
	void setPosition(string);
	void setId(time_t);
	void setExperience(unsigned int);
	void setMail(string);

private:
	string m_name{};
	string m_surname{};
	string m_login{};
	string m_password{};
	unsigned int m_salary{};
	string m_position{};
	unsigned int m_experience{};
	string m_mail{};
	time_t m_id{};
};
