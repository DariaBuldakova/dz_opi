#include "Employee.h"

string Employee::getName() { return m_name; }
string Employee::getSurname() { return m_surname; }
string Employee::getLogin() { return m_login; }
string Employee::getPassword() { return m_password; }
unsigned int Employee::getSalary() { return m_salary; }
string Employee::getPosition() { return m_position; }
time_t Employee::getId() { return m_id; }
unsigned int Employee::getExperience() { return m_experience; }
string Employee::getMail() { return m_mail; }

void Employee::setName(string a) { m_name = a; }
void Employee::setSurname(string a) { m_surname = a; }
void Employee::setLogin(string a) { m_login = a; }
void Employee::setPassword(string a) { m_password = a; }
void Employee::setSalary(unsigned int a) { m_salary = a; }
void Employee::setPosition(string a) { m_position = a; }
void Employee::setId(time_t a) { m_id = a; }
void Employee::setExperience(unsigned int a) { m_experience = a; }
void Employee::setMail(string a) { m_mail = a; }

Employee::Employee() {}
Employee::~Employee() {}

void Employee::print() {
	std::cout << "Фамилия:        ";
	std::cout << m_surname << endl;
	std::cout << "Имя:            ";
	std::cout << m_name << endl;
	std::cout << "Почта:          ";
	std::cout << m_mail << endl;
	std::cout << "Должность:      ";
	std::cout << m_position << endl;
	std::cout << "Оклад:          ";
	std::cout << m_salary << " руб." << endl;
	std::cout << "Стаж(лет):      ";
	std::cout << m_experience << endl;
	std::cout << "Логин:          ";
	std::cout << m_login << endl;
	std::cout << "id:             ";
	std::cout << m_id << endl;
}

ostream& operator<<(ostream& out, const Employee& e) {
	out << e.m_surname << " " << e.m_name << "(" << e.m_login << ")" << " - " << e.m_mail << " - " << e.m_position << " - " << e.m_salary << " - " << e.m_experience << " года(лет)" << " id:" << e.m_id << endl;
	return out;
}

istream& operator>>(istream& in, Employee& e)
{
	std::cout << "Фамилия:\t";
	in >> e.m_surname;
	std::cout << "Имя:\t ";
	in >> e.m_name;

	string str{};
	bool condition = false;
	while (condition == 0)
	{
		std::cout << "Почта:\t ";
		in >> str;
		if (str.find("@") != -1 && str.find(".") != -1) {
			condition = 1;
			e.m_mail = str;
		}
		else
			std::cout << "Вы ввели почту неправильно. Попробуйте еще раз" << endl;
	}
	condition = 0;

	std::cout << "Должность:\t";
	in >> e.m_position;

	while(condition == 0)
	{
		std::cout << "Оклад:\t ";
		in >> str;
		int k = 1, value{};
		unsigned int res{};
		if (str[0] > '0' && str[0] <= '9' ) {
			for (int i = 1; i < str.length(); i++)
			{
				if (str[i] >= '0' && str[i] <= '9')
				{
					k++;
					continue;
				}
				else 
				{
					std::cout << "Вы ввели оклад неправильно. Попробуйте еще раз" << endl;
					break;
				}
			}
		}
		else
		{
			std::cout << "Вы ввели оклад неправильно. Попробуйте еще раз" << endl;
		}

		if (k == str.length())
		{
			for (int i = 0; i < str.length(); i++)
			{
				switch (str[i])
				{
				case '0': {value = 0; break; }
				case '1': {value = 1; break; }
				case '2': {value = 2; break; }
				case '3': {value = 3; break; }
				case '4': {value = 4; break; }
				case '5': {value = 5; break; }
				case '6': {value = 6; break; }
				case '7': {value = 7; break; }
				case '8': {value = 8; break; }
				case '9': {value = 9; break; }
				default:
					break;
				}
				res = res * 10 + value;
			}
			e.m_salary = res;
			condition = 1;
		}
	}
	condition = 0;

	while (condition == 0)
	{
		std::cout << "Стаж:\t";
		in >> str;
		int k = 1, value{};
		unsigned int res{};
		if (str[0] > '0' && str[0] <= '9') {
			for (int i = 1; i < str.length(); i++)
			{
				if (str[i] >= '0' && str[i] <= '9')
				{
					k++;
					continue;
				}
				else
				{
					std::cout << "Вы ввели стаж неправильно. Попробуйте еще раз" << endl;
					break;
				}
			}
		}
		else
		{
			std::cout << "Вы ввели стаж неправильно. Попробуйте еще раз" << endl;
		}

		if (k == str.length())
		{
			for (int i = 0; i < str.length(); i++)
			{
				switch (str[i])
				{
				case '0': {value = 0; break; }
				case '1': {value = 1; break; }
				case '2': {value = 2; break; }
				case '3': {value = 3; break; }
				case '4': {value = 4; break; }
				case '5': {value = 5; break; }
				case '6': {value = 6; break; }
				case '7': {value = 7; break; }
				case '8': {value = 8; break; }
				case '9': {value = 9; break; }
				default:
					break;
				}
				res = res * 10 + value;
			}
			e.m_experience = res;
			condition = 1;
		}
	}
	condition = 0;

	std::cout << "Логин:\t";
	in >> e.m_login;
	std::cout << "Пароль:\t";
	in >> e.m_password;

	e.m_id = time(0);

	return in;
}
