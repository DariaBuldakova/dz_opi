#include <iostream>
#include <stdlib.h>
#include "CMenu.h"
#include <clocale>
#include "EmployeeFunctions.h"
using namespace std;
using namespace OPI;

int main()
{
	setlocale(LC_ALL, "");
	system("chcp 1251");
	system("cls");

    inVector();

	char* title = (char*)"����� ����������!";

	ItemMenu items[6]{ ItemMenu((char*)"Добавить сотрудника", addEmployee),
	ItemMenu((char*)"Посмотреть информацию обо всех сотрудниках", watchAllEmployees),
	ItemMenu((char*)"Посмотреть информацию об одном сотруднике", watchOneEmployee),
	ItemMenu((char*)"Удалить сотрудника", deleteEmployee),
	ItemMenu((char*)"Сортировать", sort),
	ItemMenu((char*)"Найти сотрудника(ов)", search),
	ItemMenu((char*)"Удалить сотрудника", deleteEmployee)};
	
	CMenu menu(title, items, 6);
	
    while (menu.runCommand()) { system("cls"); };
	
	inFile();
	
	return 0;
}